package com.got.nisopolis.niopolis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public Button btnTab, btnIgr;

    public void OpenTabla()
    {
        btnTab=(Button)findViewById(R.id.btnTabla);

        btnTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tabla = new Intent(MainActivity.this, BrojIgraca.class);
                startActivity(tabla);
            }
        });

    }

    public void OpenIgrac()
    {
        btnIgr=(Button)findViewById(R.id.btnIgrac);

        btnIgr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent igrac = new Intent(MainActivity.this, Igrac.class);
                startActivity(igrac);
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        OpenTabla();
        OpenIgrac();

    }

}
