package com.got.nisopolis.niopolis.Model;

/**
 * Created by ProBook on 4/16/2017.
 */

public class Nekretnina extends Kartica {

    protected String naziv;
    protected long cena;
    protected long dadzbina;
    protected long cena_kuce;
    protected long cena_hotela;
    protected int broj_kuca;
    protected boolean daLiImaHotel;

    public Nekretnina()
    {
        broj_kuca=0;
        daLiImaHotel=false;
    }

    public String getNaziv()
    {
        return naziv;
    }
    public long getCena() {
        return cena;
    }

    public long getDadzbina() {
        return dadzbina;
    }

    public long getCena_kuce() {
        return cena_kuce;
    }

    public long getCena_hotela() {
        return cena_hotela;
    }

    public int getBroj_kuca() {
        return broj_kuca;
    }

    public boolean isDaLiImaHotel() {
        return daLiImaHotel;
    }

    public void setBroj_kuca(int broj_kuca) {
        this.broj_kuca = broj_kuca;
    }

    public void setDaLiImaHotel(boolean daLiImaHotel) {
        this.daLiImaHotel = daLiImaHotel;
    }

}
