package com.got.nisopolis.niopolis.Model;

/**
 * Created by TAMARA on 16.04.2017..
 */

public class ObicnoPolje extends Polje {

    protected Kartica kartica;
    protected boolean free;
    protected Player player;

    public ObicnoPolje(Kartica k)
    {
        free=true;
        player=null;
        kartica=k;
    }

    Kartica getKartica()
    {
        return kartica;
    }
    boolean IsFree()
    {
        return free;
    }
    void Zauzmi(Player p)
    {
        if(kartica==null)
        {
            free=false;
            player=p;
        }
    }
 /*   Integer getIdPolja()
    {
        try {
            if(kartica!=null)
                return kartica;
            else
                throw new Exception("Kartica ne postoji!");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }*/
}
