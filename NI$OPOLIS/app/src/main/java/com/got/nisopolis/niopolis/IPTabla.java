package com.got.nisopolis.niopolis;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class IPTabla extends AppCompatActivity {

    Button buttonZaStart;
    String IP;
    TextView tvIP;


    public void Start()
    {
        buttonZaStart=(Button)findViewById(R.id.btnStart);
        buttonZaStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent tabla = new Intent(IPTabla.this, Prava_Tabla.class);
                startActivity(tabla);
            }
        });
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iptabla);


        tvIP=(TextView)findViewById(R.id.twIP);
        tvIP.setText(getIntent().getExtras().toString());

        Start();
    }
}
