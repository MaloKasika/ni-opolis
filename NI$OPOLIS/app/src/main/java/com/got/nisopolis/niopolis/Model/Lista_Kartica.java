package com.got.nisopolis.niopolis.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ProBook on 4/16/2017.
 */

public class Lista_Kartica {

    private static volatile Lista_Kartica instance=null;
    protected List<Kartica> lista_kartica;

    protected Lista_Kartica()
    {
        lista_kartica=new ArrayList<Kartica>();

        lista_kartica.add(new Nekretnina(){{tag=1; naziv="TC Kalca"; cena=6000; dadzbina=200; cena_kuce=5000; cena_hotela=7500;}});
        lista_kartica.add(new Nekretnina(){{tag=1; naziv="TC Zona"; cena=6000; dadzbina=400; cena_kuce=5000; cena_hotela=7500;}});

        lista_kartica.add(new JavnaPreduzeca(){{tag=9; naziv="Aerodrom"; cena=20000; dadzbina=2500; maxPoseda=4;}});

        lista_kartica.add(new Nekretnina(){{tag=2; naziv="Trg Kralja Aleksandra"; cena=10000; dadzbina=600; cena_kuce=5000; cena_hotela=7500;}});
        lista_kartica.add(new Nekretnina(){{tag=2; naziv="Trg Milana Obrenovica"; cena=10000; dadzbina=600; cena_kuce=5000; cena_hotela=7500;}});
        lista_kartica.add(new Nekretnina(){{tag=2; naziv="Trg Republike"; cena=12000; dadzbina=800; cena_kuce=5000; cena_hotela=7500;}});

        lista_kartica.add(new Nekretnina(){{tag=3; naziv="Obrenoviceva"; cena=14000; dadzbina=1000; cena_kuce=10000; cena_hotela=15000;}});
                lista_kartica.add(new JavnaPreduzeca(){{tag=10; naziv="EPS"; cena=15000; dadzbina=2000; maxPoseda=2;}});
        lista_kartica.add(new Nekretnina(){{tag=3; naziv="Dusanova"; cena=14000; dadzbina=1000; cena_kuce=10000; cena_hotela=15000;}});
        lista_kartica.add(new Nekretnina(){{tag=3; naziv="Obilicev venac"; cena=16000; dadzbina=1200; cena_kuce=10000; cena_hotela=15000;}});

        lista_kartica.add(new JavnaPreduzeca(){{tag=9; naziv="Zeleznicka stanica"; cena=20000; dadzbina=2500; maxPoseda=4;}});

        lista_kartica.add(new Nekretnina(){{tag=4; naziv="Cair"; cena=18000; dadzbina=1400; cena_kuce=10000; cena_hotela=15000;}});
        lista_kartica.add(new Nekretnina(){{tag=4; naziv="Bubanj"; cena=18000; dadzbina=1400; cena_kuce=10000; cena_hotela=15000;}});
        lista_kartica.add(new Nekretnina(){{tag=4; naziv="Tvrdjava"; cena=20000; dadzbina=1600; cena_kuce=10000; cena_hotela=15000;}});

        lista_kartica.add(new Nekretnina(){{tag=5; naziv="Narodno pozoriste"; cena=22000; dadzbina=1800; cena_kuce=15000; cena_hotela=22500;}});
        lista_kartica.add(new Nekretnina(){{tag=5; naziv="Lutkarsko pozoriste"; cena=22000; dadzbina=1800; cena_kuce=15000; cena_hotela=22500;}});
        lista_kartica.add(new Nekretnina(){{tag=5; naziv="Dom Vojske"; cena=24000; dadzbina=2000; cena_kuce=15000; cena_hotela=22500;}});

        lista_kartica.add(new JavnaPreduzeca(){{tag=9; naziv="Autobuska stanica"; cena=20000; dadzbina=2500; maxPoseda=4;}});

        lista_kartica.add(new Nekretnina(){{tag=6; naziv="Elektronska industrija"; cena=26000; dadzbina=2200; cena_kuce=15000; cena_hotela=22500;}});
        lista_kartica.add(new Nekretnina(){{tag=6; naziv="Masinska industrija"; cena=26000; dadzbina=2200; cena_kuce=15000; cena_hotela=22500;}});
                lista_kartica.add(new JavnaPreduzeca(){{tag=10; naziv="Vodovod"; cena=15000; dadzbina=2000; maxPoseda=2;}});
        lista_kartica.add(new Nekretnina(){{tag=6; naziv="Duvanska industrija"; cena=28000; dadzbina=2400; cena_kuce=15000; cena_hotela=22500;}});

        lista_kartica.add(new Nekretnina(){{tag=7; naziv="Bulevar Dr. Zorana Djindjica"; cena=30000; dadzbina=2600; cena_kuce=20000; cena_hotela=30000;}});
        lista_kartica.add(new Nekretnina(){{tag=7; naziv="Bulevar Nemanjica"; cena=30000; dadzbina=2600; cena_kuce=20000; cena_hotela=30000;}});
        lista_kartica.add(new Nekretnina(){{tag=7; naziv="Bulevar 12. februar"; cena=32000; dadzbina=2800; cena_kuce=20000; cena_hotela=30000;}});

        lista_kartica.add(new JavnaPreduzeca(){{tag=9; naziv="Taxi"; cena=20000; dadzbina=2500; maxPoseda=4;}});

        lista_kartica.add(new Nekretnina(){{tag=8; naziv="Naselje Beverli Hils"; cena=35000; dadzbina=3500; cena_kuce=20000; cena_hotela=30000;}});
        lista_kartica.add(new Nekretnina(){{tag=8; naziv="Kazandzijsko sokace"; cena=40000; dadzbina=5000; cena_kuce=20000; cena_hotela=30000;}});
    }

    public static Lista_Kartica getInstance()
    {
        if(instance==null) {

            synchronized (Lista_Kartica.class) {

                if(instance==null){
                    instance=new Lista_Kartica();
                }
            }
        }
        return instance;
    }


    public Kartica getKartica(int poz)
    {
        return lista_kartica.get(poz);
    }
    public Kartica getKarticaIme(String name)
    {
        for(int i=0;i<instance.lista_kartica.size();i++)
        {
            Kartica k=instance.lista_kartica.get(i);
            if(k instanceof Nekretnina)
            {
                Nekretnina n=(Nekretnina) k;
                if(n.getNaziv().equals(name))
                {
                    return k;
                }
            }
            else
            {
                JavnaPreduzeca jp=(JavnaPreduzeca)k;
                if(jp.getNaziv().equals(name))
                {
                    return k;
                }
            }
        }
        return null;
    }
    public int getPozicijaKartice(String ime)
    {
        for(int i=0;i<instance.lista_kartica.size();i++)
        {
            Kartica k=instance.lista_kartica.get(i);
            if(k instanceof Nekretnina)
            {
                Nekretnina n=(Nekretnina) k;
                if(n.getNaziv().equals(ime))
                {
                    return i;
                }
            }
            else
            {
                JavnaPreduzeca jp=(JavnaPreduzeca)k;
                if(jp.getNaziv().equals(ime))
                {
                    return i;
                }
            }
        }
        return -1;
    }
    public int getSize()
    {
        return  lista_kartica.size();
    }

}
